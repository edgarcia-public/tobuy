const jwt = require("jsonwebtoken");
const User = require("../models/User");

const requireAuth = async (req, res, next) => {
	const { authorization } = req.headers;

	try {

		if (!authorization) throw Error("Unauthorized request. Please log in.");

		const token = authorization.split(" ")[1];

		if (!token) throw Error("Unauthorized request. Please log in.");

		const { _id } = jwt.verify(token, process.env.SECRET);

		req.user = _id;

		if (_id) {
			const { id } = await User.findOne({ _id: _id });

			next();
		} else {
			throw Error("Unauthorized request. Please log in.");
		}
	} catch (error) {
		res.status(401).send(error.message);
	}
}

module.exports = requireAuth;