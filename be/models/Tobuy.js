const mongoose = require("mongoose");

const Tobuy = new mongoose.Schema({
	title: { type: String, required: true, minLength: 3 },
	store: { type: String },
	tobuys: [{ type: Object }],
	collapsed: { type: Boolean, Enumerator: [true, false], default: false },
	finished: { type: Boolean, Enumerator: [true, false], default: false },
	doneCount: { type: Number },
	status: { type: String, Enumerator: ["active, draft, remove"], default: "active" },
	private: { type: Boolean, Enumerator: [true, false], default: true },
	shared: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	created_at: { type: Date, default: Date.now },
	updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model("Tobuy", Tobuy);
