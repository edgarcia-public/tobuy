const mongoose = require("mongoose");

const User = new mongoose.Schema({
	uid: { type: String },
	first_name: { type: String, maxLength: 8 },
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true, minLength: 8 },
	country: { type: String },
	tobuy_lists: [{ type: mongoose.Schema.Types.ObjectId, ref: "Tobuy" }],
	created_at: { type: Date, default: Date.now },
	updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model("User", User);
