const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();

require("dotenv").config();
const PORT = process.env.PORT;
const DB_URI = process.env.DB_URI;

// Connect to MongoDB
mongoose.connect(DB_URI)
	.then(() => console.log("DB Connected!"))
	.catch(err => console.log(err))

app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const users = require("./routes/users");
const tobuys = require("./routes/tobuys");

app.use("/users", users);
app.use("/tobuys", tobuys);

app.listen(PORT, () => console.log("Server is up!"));