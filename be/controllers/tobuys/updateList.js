const Tobuy = require("../../models/Tobuy");
const User = require("../../models/User");

const updateList = async (req, res) => {
	const id = req.user;
	const { query } = req.body;
	const { updated } = req.body;
	const onEditMode = req.headers?.oneditmode;

	try {
		let updatedTobuyList = "";

		updatedTobuyList = await Tobuy.findOneAndUpdate(query, updated, { new: true }).select("-status -private -created_at -updated_at -__v")

		if (!updatedTobuyList) throw Error("Faild to submit ToBuy. Pleaser refresh the page.");

		const isFinished = updatedTobuyList.tobuys.filter(item => {
			if (!item.done) {
				return false;
			} else {
				return true;
			}
		});

		updatedTobuyList = await Tobuy.findOneAndUpdate(query, { $set: { doneCount: isFinished.length } }, { new: true });

		if (isFinished.length === updatedTobuyList.tobuys.length) {
			updatedTobuyList = await Tobuy.findOneAndUpdate(query, { $set: { finished: true } }, { new: true });
		} else {
			updatedTobuyList = await Tobuy.findOneAndUpdate(query, { $set: { finished: false } }, { new: true });
		}

		if (onEditMode) {
			res.status(200).send(updatedTobuyList);
		} else {
			const data = await User.findOne({ _id: id }, "tobuy_lists")
				.populate("tobuy_lists");

			const sorted = data.tobuy_lists.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

			res.status(200).send(sorted);
		}

	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = updateList;