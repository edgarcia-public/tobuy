const Tobuy = require("../../models/Tobuy");

const deleteOne = async (req, res) => {
	const { query } = req.body;

	try {
		const data = await Tobuy.deleteOne(query);

		if (!data) throw Error("Something went wrong. Please refresh the page and retry.");

		res.status(200).send(true);
	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = deleteOne