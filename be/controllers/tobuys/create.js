const Tobuy = require("../../models/Tobuy");
const User = require("../../models/User");

const create = async (req, res) => {
	const id = req.user;
	const { formData, updateThisID } = req.body;

	try {

		// update existing tobuy
		if (updateThisID) {
			const updatedTobuy = await Tobuy.findOneAndUpdate({ _id: updateThisID }, { ...formData }, { new: true });

			delete updatedTobuy.private
			delete updatedTobuy.shared
			delete updatedTobuy.__v
			delete updatedTobuy.created

			res.status(200).send(updatedTobuy);
		} else {

			// new. save new tobuy then update user's tobuy_lists
			const insert = new Tobuy({ ...formData, doneCount: 0 });

			const newTobuy = await insert.save();

			if (!newTobuy) throw Error("Faild to submit ToBuy. Please refresh the page.");

			const newTobuyID = newTobuy._id;

			await User.findOneAndUpdate(
				{ _id: id },
				{ $push: { "tobuy_lists": newTobuyID } },
				{ new: true }
			)

			delete newTobuy.private
			delete newTobuy.shared
			delete newTobuy.__v
			delete newTobuy.created

			res.status(200).send(newTobuy);
		}
	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = create;