const User = require("../../models/User");
const Tobuy = require("../../models/Tobuy");

const getAll = async (req, res) => {
	const id = req.user;

	try {
		let updatedTobuy = "";

		const data = await User.findOne({ _id: id }, "tobuy_lists -_id")
			.populate({ path: "tobuy_lists", sort: { created_at: "desc" } })

		const sorted = data.tobuy_lists.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

		if (data) {
			res.status(200).send(sorted);
		} else {
			res.status(200).send(null);
		}
	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = getAll