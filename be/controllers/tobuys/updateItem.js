const Tobuy = require("../../models/Tobuy");
const User = require("../../models/User");

const updateItem = async (req, res) => {
	const id = req.user;
	const queryTobuyItem = { _id: req.body._id, "tobuys.id": req.body.itemID }
	const { updated } = req.body;
	const queryTobuyList = { _id: req.body._id }

	try {
		let updatedTobuyList = "";

		updatedTobuyList = await Tobuy.findOneAndUpdate(queryTobuyItem, { $set: updated }, { new: true });

		if (!updatedTobuyList) throw Error("Faild to submit ToBuy. Pleaser refresh the page.");

		await Tobuy.findOneAndUpdate(queryTobuyList, { updated_at: new Date() }, { new: true });

		const isFinished = updatedTobuyList.tobuys.filter(item => {
			if (!item.done) {
				return false;
			} else {
				return true;
			}
		})

		updatedTobuyList = await Tobuy.findOneAndUpdate(queryTobuyList, { $set: { doneCount: isFinished.length } }, { new: true });

		if (isFinished.length === updatedTobuyList.tobuys.length) {
			updatedTobuyList = await Tobuy.findOneAndUpdate(queryTobuyList, { $set: { finished: true } }, { new: true });
		} else {
			updatedTobuyList = await Tobuy.findOneAndUpdate(queryTobuyList, { $set: { finished: false } }, { new: true });
		}

		const data = await User.findOne({ _id: id }, "tobuy_lists")
			.populate("tobuy_lists");

		const sorted = data.tobuy_lists.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

		res.status(200).send(sorted);
	} catch (error) {
		if (error) {
			res.status(400).send(error?.message ? error.message : "Something went wrong. Please refresh the page.");
		}
	}
}

module.exports = updateItem;