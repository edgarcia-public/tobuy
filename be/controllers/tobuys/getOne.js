const Tobuy = require("../../models/Tobuy");

const getOne = async (req, res) => {
	const query = req.query;

	try {
		const data = await Tobuy.findOne(query, "-status -collapsed -private -create_at -updated_at -__v");

		if (!data) throw Error("ToBuy List not found.");

		res.status(200).send(data);
	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = getOne