const User = require("../../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const login = async (req, res) => {
	const { email, password } = req.body;

	try {
		const data = await User.findOne({ email }, "-created_at -updated_at -__v -tobuy_lists");

		if (!data) throw Error("Email address not found. Please sign up.");

		const match = await bcrypt.compare(password, data.password);

		if (!match) throw Error("Invalid log in credentails.");

		const token = (_id) => {
			return jwt.sign({ _id }, process.env.SECRET, { expiresIn: "3d" });
		}

		// make a shallow copy
		const user = data.toJSON();

		delete user.password;
		delete user._id;

		res.status(200).send({ token: token(data._id), user });
	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = login;