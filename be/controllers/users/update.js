const User = require("../../models/User");

const update = async (req, res) => {
	const id = req.user;
	const newFormData = req.body;

	try {
		const user = await User.findOneAndUpdate({ _id: id }, { ...newFormData, updated_at: new Date() },
			{
				new: true,
				select: { password: 0, updated_at: 0, created_at: 0, __v: 0, _id: 0 }
			});

		if (!user) throw Error("Something went wrong. Pleaser refresh the page.");

		res.status(200).send(user);
	} catch (error) {
		res.status(401).send(error.message);
	}
}

module.exports = update;