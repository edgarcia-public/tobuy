const User = require("../../models/User");

const get = async (req, res) => {
	const id = req.user;

	try {
		const user = await User.findOne({ _id: id }, "-created_at -updated_at -__v -password -tobuy_lists")

		if (!user) throw Error("Unauthorized access. Please log in.");

		res.status(200).send({ user });
	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = get;