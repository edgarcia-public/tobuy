const User = require("../../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { customAlphabet } = require("nanoid");

const signup = async (req, res) => {
	const nanoid = customAlphabet("123456789abc", 6);
	const { email, password } = req.body;


	try {
		const emailExists = await User.findOne({ email }, "_id");

		if (emailExists) throw Error("Email address exists. Please log in instead.");

		const salt = await bcrypt.genSalt(3);
		const hashPassword = await bcrypt.hash(password, salt);

		const insert = new User({ email, password: hashPassword, uid: nanoid() });
		const data = await insert.save();


		if (!data) throw Error("Something went wrong. Please refresh the page then retry.");

		const token = (_id) => {
			return jwt.sign({ _id }, process.env.SECRET, { expiresIn: "3d" });
		}

		// make a shallow copy
		const user = data.toJSON();

		delete user.password;
		delete user._id;

		res.status(200).send({ token: token(data._id), user });
	} catch (error) {
		res.status(400).send(error.message);
	}
}

module.exports = signup;