const express = require("express");
const router = express.Router();
const requireAuth = require("../middleware/requireAuth");

const create = require("../controllers/tobuys/create");
const deleteOne = require("../controllers/tobuys/deleteOne");
const getAll = require("../controllers/tobuys/getAll");
const getOne = require("../controllers/tobuys/getOne");
const updateList = require("../controllers/tobuys/updateList");
const updateItem = require("../controllers/tobuys/updateItem");

router.post("/create", requireAuth, create);
router.post("/delete", requireAuth, deleteOne);
router.get("/get-all", requireAuth, getAll);
router.get("/get-one", requireAuth, getOne);
router.patch("/update-list", requireAuth, updateList);
router.patch("/update-item", requireAuth, updateItem);

module.exports = router;