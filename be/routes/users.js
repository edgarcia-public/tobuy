const express = require("express");
const router = express.Router();
const requireAuth = require("../middleware/requireAuth");

const login = require("../controllers/users/login");
router.post("/log-in", login);

const signup = require("../controllers/users/signup");
router.post("/sign-up", signup);


// middleware
const update = require("../controllers/users/update");
router.patch("/update", requireAuth, update);

const get = require("../controllers/users/get");
router.get("/get-user", requireAuth, get);

module.exports = router;