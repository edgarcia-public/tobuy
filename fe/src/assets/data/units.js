export const units = [
	{ name: "Piece" },
	{ name: "Kilo" },
	{ name: "Pound" },
	{ name: "Gram" },
	{ name: "Galon" },
	{ name: "Liter" },
	{ name: "Meter" },
	{ name: "Centimeter" },
	{ name: "Foot" },
	{ name: "Inch" },
	{ name: "Pack" },
	{ name: "Watt" },
]