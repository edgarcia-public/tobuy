import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import "./assets/css/global.css";
import { SessionContextProvider } from './context/SessionContext';
import { TobuyContextProvider } from './context/TobuyContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <SessionContextProvider>
            <TobuyContextProvider>
                <App />
            </TobuyContextProvider>
        </SessionContextProvider>
    </React.StrictMode>
);