import { createBrowserRouter, RouterProvider, redirect } from "react-router-dom";
import { useSession } from "./helpers/hooks/context/useSession";
import HomeLayout from "./layouts/HomeLayout";
import MainLayout from "./layouts/MainLayout";
import Profile from "./pages/profile/Profile";
import ToBuyForm from "./pages/tobuy/add/ToBuyForm";
import MyToBuys from "./pages/tobuy/MyToBuys";
import LogIn from "./pages/home/LogIn";
import SignUp from "./pages/home/SignUp";
import Alert from "./global/Alert";

const App = () => {
    const { sessionDispatch, session, isAlert } = useSession();
    const localSession = localStorage.getItem("session");

    const router = createBrowserRouter([
        {
            path: "/",
            element: <HomeLayout />,
            children: [
                {
                    path: "",
                    loader: () => (localSession || session) ? redirect(`/u/${localSession && JSON.parse(localSession).uid}`) : null,
                    element: <LogIn />,
                },
                {
                    path: "log-in",
                    loader: () => (localSession || session) ? redirect(`/u/${localSession && JSON.parse(localSession).uid}`) : null,
                    element: <LogIn />,
                },
                {
                    path: "sign-up",
                    loader: () => (localSession || session) ? redirect(`/u/${localSession && JSON.parse(localSession).uid}`) : null,
                    element: <SignUp />,
                },
                {
                    path: "password-reset",
                    element: "",
                },
            ]
        },
        {
            path: "u/:_id",
            element: <MainLayout />,
            children: [
                {
                    path: "",
                    loader: () => (localSession || session) ? null : redirect("/"),
                    element: <MyToBuys />,
                },
                {
                    path: "profile",
                    loader: () => (localSession || session) ? null : redirect("/"),
                    element: <Profile />,
                },
                {
                    path: "tobuy",
                    children: [
                        {
                            path: "add",
                            loader: () => (localSession || session) ? null : redirect("/"),
                            element: <ToBuyForm />
                        },
                    ]
                },
                {
                    path: "tobuy",
                    children: [
                        {
                            path: "edit/:_id",
                            loader: () => !session ? redirect("/") : null,
                            element: <ToBuyForm />
                        },
                    ]
                }
            ]
        },
    ])

    if (session === null) {
        return <div className="container"><p>Loading...</p></div>
    }

    if (isAlert) {
        setTimeout(() => {
            sessionDispatch({ type: "ISALERT", payload: null });
        }, 5000);
    }

    return (
        <section className="app">
            {isAlert && <Alert message={isAlert} />}
            <RouterProvider router={router} />
        </section>
    );
}

export default App;
