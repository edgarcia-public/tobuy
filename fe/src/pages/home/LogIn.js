import "../../assets/css/register.css";
import loginImage from "../../assets/images/utils/login.jpg";
import { useRef } from "react";
import { NavLink } from "react-router-dom";
import { MdArrowForwardIos } from "react-icons/md";
import { RiLoader4Fill } from "react-icons/ri";
import { useLogin } from "../../helpers/hooks/auth/useLogin";

const LogIn = () => {
	const { doLogin, isLoading } = useLogin();
	const inputEmail = useRef();
	const inputPassword = useRef();

	const handleSubmit = async (e) => {
		e.preventDefault();
		const formData = {
			email: inputEmail.current.value,
			password: inputPassword.current.value,
		}

		await doLogin(formData);
	}

	document.title = "Log In | BuyMe";
	return (
		<section className="login">
			<div className="container">
				<div className="panel">
					<img src={loginImage} alt="" className="panel-image" />
					<div className="panel-body">
						<h2 style={{ marginTop: 0 }}>Welcome back!</h2>
						<form onSubmit={handleSubmit}>
							<div className="input-parent">
								<label>Email Address</label>
								<input type="text" ref={inputEmail} placeholder="jarvis@robot.com" />
							</div>
							<div className="input-parent">
								<label>Password</label>
								<input type="password" ref={inputPassword} placeholder="********" />
							</div>

							<div className="half-flex fixed-flex">
								<NavLink to={"/sign-up"} className="navlink"><small>No account yet?</small></NavLink>
								<button type="submit" className="btn bg-theme" onClick={handleSubmit} disabled={isLoading}>
									{
										isLoading ?
											<RiLoader4Fill className="loading" />
											:
											<>Log In <MdArrowForwardIos style={{ marginLeft: 5 }} /></>
									}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	)
}

export default LogIn;