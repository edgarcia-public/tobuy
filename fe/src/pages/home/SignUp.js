import "../../assets/css/register.css";
import loginImage from "../../assets/images/utils/login.jpg";
import { RiLoader4Fill } from "react-icons/ri";
import { useRef } from "react";
import { NavLink } from "react-router-dom";
import { MdArrowForwardIos } from "react-icons/md";
import { useSignup } from "../../helpers/hooks/auth/useSignup";

const SignUp = () => {
	const { doSignup, isLoading } = useSignup();
	const inputEmail = useRef();
	const inputPassword = useRef();
	const inputConfirm = useRef();
	const inputTerms = useRef();

	const handleSubmit = async (e) => {
		e.preventDefault();
		const formData = {
			email: inputEmail.current.value,
			password: inputPassword.current.value,
			confirm: inputConfirm.current.value,
			terms: inputTerms.current.checked,
		}

		await doSignup(formData);
	}

	document.title = "Sign Up | BuyMe";
	return (
		<section className="login">
			<div className="container">
				<div className="panel">
					<img src={loginImage} alt="" className="panel-image" />
					<div className="panel-body">
						<h2 style={{ marginTop: 0 }}>Come on in!</h2>
						<form onSubmit={handleSubmit}>
							<div className="input-parent">
								<label>Email Address</label>
								<input type="text" ref={inputEmail} placeholder="jarvis@robot.com" />
							</div>
							<div className="input-parent">
								<label>Password</label>
								<input type="password" ref={inputPassword} placeholder="********" />
							</div>
							<div className="input-parent">
								<label>Confirm Password</label>
								<input type="password" ref={inputConfirm} placeholder="********" />
							</div>

							<div className="input-parent">
								<label className="center-content" style={{ textTransform: "none" }}>
									<input type="checkbox" ref={inputTerms} style={{ minHeight: "unset", marginRight: 8 }} /> Agree to Terms & Use
								</label>
							</div>

							<div className="half-flex fixed-flex">
								<NavLink to={"/log-in"} className="navlink"><small>Log in instead?</small></NavLink>
								<button type="submit" className="btn bg-success" onClick={handleSubmit} disabled={isLoading}>
									{
										isLoading ?
											<RiLoader4Fill className="loading" />
											:
											<>Sign Up <MdArrowForwardIos style={{ marginLeft: 5 }} /></>
									}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	)
}

export default SignUp;