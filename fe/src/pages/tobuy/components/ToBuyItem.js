import { useState } from "react";
import { GoCheckCircle, GoCheckCircleFill } from "react-icons/go";
import { Tooltip } from "react-tooltip"
import { useUpdateTobuyItem } from "../../../helpers/hooks/tobuy/useUpdateTobuyItem";
import { useParams } from "react-router-dom";
import { RiLoader4Fill } from "react-icons/ri";

const ToBuyItem = ({
	listID,
	item,
	editIndex,
	modify = false,
	handleEdit = false,
	handleRemove }) => {
	const [readMore, setReadMore] = useState(false);
	const { doUpdate, isLoading } = useUpdateTobuyItem();
	const { _id } = useParams();

	const handleDone = async ({ id, done }) => {
		const data = {
			_id: listID,
			itemID: id,
			updated: { "tobuys.$.done": done }
		}

		await doUpdate(data);
	}

	const unit = () => {
		if (item.quantity > 1) {

			if (item.unit === "Inch") return item.unit + "es";

			if (item.unit === "Foot") return "Feet";

			return item.unit + "s";
		}

		return item.unit;
	}

	return (
		<div className={`tobuy-item ${item.id}`}>
			<div className="tobuy-item-preview">
				<button className={`tobuy-item-done btn-sm ${item.done}`}
					data-tooltip-content={item.done ? "Undone" : "Done"}
					data-tooltip-place="right"
					data-tooltip-id={item.id}
					data-tooltip-float={true}
					onClick={(_id.length > 6 && !isLoading) ? undefined : e => handleDone({ id: item.id, done: !item.done })}
				>
					{isLoading ? <RiLoader4Fill className="loading" /> : item.done ? <GoCheckCircleFill /> : <GoCheckCircle />}
				</button>
				{_id.length > 6 ? undefined : <Tooltip id={item.id} />}

				<div className="tobuy-item-info">
					<div className="tobuy-item-info-grid">
						<span className={`one-line-ellipsis tobuy-item-product-title ${item.done}`}><b>{item.product}</b></span>
						<div className="tobuy-other-info">
							{item.quantity && <span className="capsule">{`${item.quantity} : ${unit()}`}</span>}
						</div>
					</div>
					{
						item.description ?
							<div className="tobuy-item-desc">
								<p className={`tobuy-item-desc-content ${item.done}`}>
									<small className={readMore ? undefined : "one-line-ellipsis"} style={{ margin: "4px 10px 0 0" }}>{item.description}</small>

									{
										item.description.length > 61 && <small className="color-link cursor-pointer" onClick={() => setReadMore(!readMore)}>{readMore ? "Read Less" : "Read More"}</small>
									}

								</p>
							</div>
							:
							undefined
					}

					{
						(modify && !item.done) ?
							<div className="tobuy-item-modify">
								<small className={editIndex !== null ? "color-gray cursor-pointer" : "color-danger cursor-pointer"}
									onClick={() => handleRemove(item.id)}>Remove</small>
								<span className="color-gray"> | </span>
								<small className={editIndex !== null ? "cursor-pointer color-gray" : "cursor-pointer color-link"} onClick={editIndex !== null ? undefined : () => handleEdit(item.id)}>Edit</small>
							</div>
							:
							undefined
					}
				</div>
			</div>
		</div>
	)
}

export default ToBuyItem;