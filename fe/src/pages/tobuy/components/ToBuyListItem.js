import "../../../assets/css/tobuyitem.css";
import { NavLink } from "react-router-dom";
import { Tooltip } from "react-tooltip";
import { RiEdit2Line, RiExpandUpDownLine } from "react-icons/ri";
import { TfiMapAlt } from "react-icons/tfi";
import { useUpdateTobuyList } from "../../../helpers/hooks/tobuy/useUpdateTobuyList";
import ToBuyItem from "./ToBuyItem";
import Confetti from 'react-dom-confetti';
import { RiLoader4Fill } from "react-icons/ri";

const ToBuyListItem = ({ tobuyList }) => {
	const { doUpdate, isLoading } = useUpdateTobuyList();

	const handleCollapse = async (id) => {
		const data = {
			query: { _id: id },
			updated: { collapsed: !tobuyList.collapsed }
		}

		await doUpdate(data);
	}

	const config = {
		angle: "100",
		spread: "100",
		startVelocity: "40",
		elementCount: "80",
		dragFriction: "0.35",
		duration: "800",
		stagger: "19",
		width: "10px",
		height: "10px",
		perspective: "500px",
		colors: ["#ff0000", "#ffa500", "#ffff00", "#008000", "#0000ff", "#4b0082", "#ee82ee"]
	};

	return (
		<div className="tobuy-list-item">
			<div className={`panel tobuy-list-parent`}>
				<div className={`panel-header ${tobuyList && (tobuyList.collapsed ? "collapsed" : "")}`}>
					<div className="half-flex fixed-flex">
						<div className="flex-item" style={{ display: "flex", alignItems: "center", gap: 11 }}>
							{
								isLoading ?
									<RiLoader4Fill className="loading" />
									:
									<RiExpandUpDownLine
										style={{ color: "#333", cursor: "pointer" }}
										data-tooltip-id="tooltip_collapse"
										data-tooltip-place="top"
										data-tooltip-content={tobuyList && (tobuyList.collapsed ? "Expand" : "Collapse")}
										data-tooltip-float={true}
										onClick={() => handleCollapse(tobuyList._id)}
									/>
							}

							<Tooltip id="tooltip_collapse" />
							<p className="nogaps one-line-ellipsis tobuy-list-title">{tobuyList && tobuyList.title}</p>
						</div>
						<div className="flex-item">
							<NavLink to={`tobuy/edit/${tobuyList && tobuyList._id}`} className="btn btn-sm"
								data-tooltip-id="tooltip_edit"
								data-tooltip-place="top"
								data-tooltip-content="Edit"
								data-tooltip-float={true}
							>
								<RiEdit2Line />
							</NavLink>
							<Tooltip id="tooltip_edit" />
						</div>
					</div>
				</div>

				<div className={`panel-body ${tobuyList && (tobuyList.collapsed ? "collapsed" : "")}`}>
					<div className={`tobuyList-item-overflow ${tobuyList.finished ? "active" : undefined}`}>
						<Confetti className="confetti" active={tobuyList.finished} config={config} />
						{
							(tobuyList && tobuyList.tobuys) &&
							tobuyList.tobuys.map((item, index) =>
								<ToBuyItem
									key={index}
									listID={tobuyList._id}
									item={item}
								/>)
						}
					</div>
				</div>

				<div className={`panel-footer ${tobuyList && (tobuyList.collapsed ? "collapsed" : "")}`}>
					<div className="center-content">

						{
							tobuyList?.store &&
							<>
								<NavLink to={`https://www.google.com/maps/search/${tobuyList && (tobuyList.store.toLowerCase())}`} target="_new" className="btn color-black"
									data-tooltip-id="tooltip_map"
									data-tooltip-place="top"
									data-tooltip-content="Store Map"
									data-tooltip-float={true}
									style={{ marginRight: 10 }}
								>
									<TfiMapAlt className="icon-md" />
								</NavLink>
								<Tooltip id="tooltip_map" />
							</>
						}

						<span className="capsule"
							data-tooltip-id="tooltip_count"
							data-tooltip-place="top"
							data-tooltip-content="Bought / Pending"
							data-tooltip-float={true}
						>
							Done: {tobuyList?.doneCount && tobuyList.doneCount} of {tobuyList?.tobuys && tobuyList?.tobuys.length}
						</span>
						<Tooltip id="tooltip_count" />
					</div>
				</div>
			</div>
		</div>
	)
}

export default ToBuyListItem;