import { customAlphabet } from "nanoid";

export const addNewTobuyItem = (formData, editIndex, tobuyItem, formDataEdit, tobuyItemEdit, onEditMode) => {
	const nanoid = customAlphabet("123456789emavadona", 8);

	if (editIndex === null) {
		let tobuyItems = [];

		if (onEditMode) {
			tobuyItems = [...formDataEdit.tobuys, { ...tobuyItemEdit, unit: tobuyItemEdit.unit ? tobuyItemEdit.unit : "Piece" }];
		} else {

			if (formData.tobuys.length) {
				const newTobuyItem = {
					...tobuyItem,
					created: new Date(),
					unit: tobuyItem.unit ? tobuyItem.unit : "Piece",
					id: nanoid(),
					done: false,
				}
				tobuyItems = [...formData.tobuys, newTobuyItem];
			} else {
				const newTobuyItem = {
					...tobuyItem,
					created: new Date(),
					unit: tobuyItem.unit ? tobuyItem.unit : "Piece",
					id: nanoid(),
					done: false,
				}

				tobuyItems = [newTobuyItem];
			}
		}

		return tobuyItems;
	}

	if (editIndex >= 0) {
		let existingTobuys = [];

		if (onEditMode) {
			// make shallow copy
			existingTobuys = [...formDataEdit.tobuys];

			// replace the edited tobuy item to its original index
			existingTobuys.splice(editIndex, 1, { ...tobuyItemEdit, unit: tobuyItemEdit.unit ? tobuyItemEdit.unit : "Piece" });

			return existingTobuys;
		}

		// make shallow copy
		existingTobuys = [...formData.tobuys];

		// replace with new data
		existingTobuys.splice(editIndex, 1, { ...tobuyItem, unit: tobuyItem.unit ? tobuyItem.unit : "Piece" });

		return existingTobuys;
	}
}