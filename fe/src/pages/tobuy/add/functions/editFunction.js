export const editFunction = (id, setEditIndex, formData, setFormData, setTobuyItem, formDataEdit, setFormDataEdit, setTobuyItemEdit, onEditMode) => {

	if (onEditMode) {
		const tobuyItemIndex = formDataEdit.tobuys.findIndex(item => item.id === id);
		const tobuyItemEdit = formDataEdit.tobuys[tobuyItemIndex];

		if (tobuyItemEdit !== null) {
			setEditIndex(tobuyItemIndex);
			setTobuyItemEdit(tobuyItemEdit);

			const tobuyItemsWithEditing = formDataEdit.tobuys.map((tobuyItem, index) => index === tobuyItemIndex ? { product: "Editing..." } : { ...tobuyItem })

			setFormDataEdit({ ...formDataEdit, tobuys: tobuyItemsWithEditing });
		}
	} else {
		const tobuyItemIndex = formData.tobuys.findIndex(item => item.id === id);
		const tobuyItemEdit = formData.tobuys[tobuyItemIndex];

		if (tobuyItemEdit !== null) {
			setEditIndex(tobuyItemIndex);
			setTobuyItem(tobuyItemEdit);

			const tobuyItemsWithEditing = formData.tobuys.map((tobuyItem, index) => index === tobuyItemIndex ? { product: "Editing..." } : { ...tobuyItem })

			setFormData({ ...formData, tobuys: tobuyItemsWithEditing });
		}
	}
}