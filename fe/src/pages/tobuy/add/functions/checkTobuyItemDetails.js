export const checkTobuyItemDetails = (tobuyItem, tobuyItemEdit, onEditMode, globalDispatch) => {

	if (onEditMode) {
		if (tobuyItemEdit.hasOwnProperty("product")) {
			if (!tobuyItemEdit.product.trim()) {
				globalDispatch({ type: "ALERT", payload: "The Product Name field is required." });

				return false;
			}
		} else {
			globalDispatch({ type: "ALERT", payload: "The Product Name field is required." });

			return false;
		}

		return true;
	} else {
		if (tobuyItem.hasOwnProperty("product")) {
			if (!tobuyItem.product.trim()) {
				globalDispatch({ type: "ALERT", payload: "The Product Name field is required." });

				return false;
			}
		} else {
			globalDispatch({ type: "ALERT", payload: "The Product Name field is required." });

			return false;
		}

		return true;
	}
}