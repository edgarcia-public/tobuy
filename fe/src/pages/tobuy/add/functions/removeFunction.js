export const removeFunction = (setFormData, formData, setFormDataEdit, formDataEdit, id, onEditMode) => {
	if (onEditMode) {
		setFormDataEdit({ ...formDataEdit, tobuys: formDataEdit.tobuys.filter(item => item.id !== id) });
	} else {
		setFormData({ ...formData, tobuys: formData.tobuys.filter(item => item.id !== id) });
	}
}