export const resetTobuyItemObj = () => {
	return {
		product: "",
		unit: "",
		quantity: "",
		description: "",
		done: "",
	}
}