export const checkTobuyItemsCount = (formData, formDataEdit, onEditMode, sessionDispatch) => {

	if (onEditMode) {
		// check Tobuy Items
		if (formDataEdit.hasOwnProperty("tobuys")) {
			if (!formDataEdit.tobuys.length) {
				sessionDispatch({ type: "ISALERT", payload: "Please add at least ONE ToBuy item." });

				return false;
			}
		} else {
			sessionDispatch({ type: "ISALERT", payload: "Please add at least ONE ToBuy item." });

			return false;
		}

		return true;
	} else {
		// check Tobuy Items
		if (formData.hasOwnProperty("tobuys")) {
			if (!formData.tobuys.length) {
				sessionDispatch({ type: "ISALERT", payload: "Please add at least ONE ToBuy item." });

				return false;
			}
		} else {
			sessionDispatch({ type: "ISALERT", payload: "Please add at least ONE ToBuy item." });

			return false;
		}

		return true;
	}
}