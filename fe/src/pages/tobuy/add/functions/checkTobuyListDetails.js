export const checkTobuyListDetails = (formData, formDataEdit, onEditMode, sessionDispatch) => {

	if (onEditMode) {
		// check for ToBuy Title
		if (formDataEdit.hasOwnProperty("title")) {
			if (formDataEdit.title.length < 4) {
				sessionDispatch({ type: "ISALERT", payload: "The Title field is required. Minimum of 3 characters." });

				return false;
			}
		} else {
			sessionDispatch({ type: "ISALERT", payload: "The Title field is required. Minimum of 3 characters." });

			return false;
		}

		return true;
	} else {
		// check for ToBuy Title
		if (formData.hasOwnProperty("title")) {
			if (formData.title.length < 4) {
				sessionDispatch({ type: "ISALERT", payload: "The Title field is required. Minimum of 3 characters." });
				return false;
			}
		} else {
			sessionDispatch({ type: "ISALERT", payload: "The Title field is required. Minimum of 3 characters." });

			return false;
		}

		return true;
	}
}