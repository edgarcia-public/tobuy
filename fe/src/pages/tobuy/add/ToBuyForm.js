import "../../../assets/css/tobuyitem.css";
import axios from "axios";
import { NavLink } from "react-router-dom";
import { units } from "../../../assets/data/units";
import { useEffect, useState } from "react";
import { resetTobuyItemObj } from "./functions/resetTobuyItemObj";
import { editFunction } from "./functions/editFunction";
import { removeFunction } from "./functions/removeFunction";
import { useCreateTobuyList } from "../../../helpers/hooks/tobuy/useCreateTobuyList";
import { checkTobuyListDetails } from "./functions/checkTobuyListDetails";
import { checkTobuyItemDetails } from "./functions/checkTobuyItemDetails";
import { addNewTobuyItem } from "./functions/addNewTobuyItem";
import { checkTobuyItemsCount } from "./functions/checkTobuyItemsCount";
import { RiArrowLeftLine } from "react-icons/ri";
import { useParams } from "react-router-dom";
import { useGetOneTobuyList } from "../../../helpers/hooks/tobuy/useGetOneTobuyList";
import { useUpdateTobuyList } from "../../../helpers/hooks/tobuy/useUpdateTobuyList";
import { useDeleteTobuyList } from "../../../helpers/hooks/tobuy/useDeleteTobuyList";
import { useTobuy } from "../../../helpers/hooks/context/useTobuy";
import { getOneTobuyListAPI } from "../../../helpers/endpoints/tobuysAPI";
import { useSession } from "../../../helpers/hooks/context/useSession";
import { RiLoader4Fill } from "react-icons/ri";
import ToBuyItem from "../components/ToBuyItem";

const ToBuyForm = () => {
	const [formData, setFormData] = useState({ title: "", store: "", tobuys: [] });
	const [formDataEdit, setFormDataEdit] = useState({ title: "", store: "", tobuys: [] });
	const [tobuyItem, setTobuyItem] = useState(resetTobuyItemObj());
	const [tobuyItemEdit, setTobuyItemEdit] = useState(resetTobuyItemObj());
	const [onEditMode, setOnEditMode] = useState(false);
	const [editIndex, setEditIndex] = useState(null);
	const { doCreate, updateThisID } = useCreateTobuyList();
	const { doUpdate } = useUpdateTobuyList();
	const { editItem } = useGetOneTobuyList();
	const { doDelete } = useDeleteTobuyList();
	const { sessionDispatch, isLoading } = useSession();
	const { _id } = useParams();
	const { tobuyDispatch } = useTobuy();
	const localSession = localStorage.getItem("session");

	useEffect(() => {
		const getEditData = async () => {
			const { token } = JSON.parse(localSession);

			const response = await axios.get(`${getOneTobuyListAPI}?_id=${_id}`, {
				headers: { Authorization: `Bearer ${token} ` }
			})
			tobuyDispatch({ type: "EDITITEM", payload: { ...response.data } });
		}

		if (_id.length > 6) {
			getEditData();
		} else {
			setOnEditMode(false);
			tobuyDispatch({ type: "EDITITEM", payload: null });
		}
	}, [_id, localSession, tobuyDispatch]);

	useEffect(() => {
		if (_id.length > 6 && editItem) {
			setFormDataEdit(editItem);
			setOnEditMode(true);
		}
	}, [_id, editItem]);

	const handleSubmitMyTobuyList = async (e) => {
		e.preventDefault();

		const tobuyDetails = checkTobuyListDetails(formData, formDataEdit, onEditMode, sessionDispatch);
		if (!tobuyDetails) return;

		const tobuyItems = checkTobuyItemsCount(formData, formDataEdit, onEditMode, sessionDispatch);
		if (!tobuyItems) return;

		if (tobuyDetails && tobuyItems) {
			if (onEditMode) {
				const data = {
					query: { _id: formDataEdit._id },
					updated: formDataEdit
				}
				await doUpdate(data, onEditMode);
			} else {
				await doCreate(formData);
			}
		}
	}

	const handleInput = (e) => {
		switch (e.target.name) {
			case "title":
			case "store":
				if (onEditMode) {
					setFormDataEdit({ ...formDataEdit, [e.target.name]: e.target.value });
				} else {
					setFormData({ ...formData, [e.target.name]: e.target.value });
				}
				break
			default:
				if (onEditMode) {
					setTobuyItemEdit({
						...tobuyItemEdit, [e.target.name]: e.target.value
					})
				} else {
					setTobuyItem({
						...tobuyItem, [e.target.name]: e.target.value
					})
				}
				break;
		}
	}

	const handleAddItem = (e) => {
		e.preventDefault();

		const tobuyItemDetails = checkTobuyItemDetails(tobuyItem, tobuyItemEdit, onEditMode, sessionDispatch);
		if (!tobuyItemDetails) return;

		const addTobuyItem = addNewTobuyItem(formData, editIndex, tobuyItem, formDataEdit, tobuyItemEdit, onEditMode);

		if (addTobuyItem) {
			if (onEditMode) {
				setFormDataEdit({ ...formDataEdit, tobuys: addTobuyItem });
				setTobuyItemEdit(resetTobuyItemObj());
			} else {
				setFormData({ ...formData, tobuys: addTobuyItem });
				setTobuyItem(resetTobuyItemObj());
			}

			setEditIndex(null);
		}
	}

	const handleEdit = (id) => {
		setEditIndex(null);

		editFunction(id, setEditIndex, formData, setFormData, setTobuyItem, formDataEdit, setFormDataEdit, setTobuyItemEdit, onEditMode);

		document.getElementById("tobuyItemForm").scrollIntoView();
	}

	const handleRemove = (id) => {
		removeFunction(setFormData, formData, setFormDataEdit, formDataEdit, id, onEditMode);
	}

	const handleDelete = async () => {
		const data = {
			query: { _id: formDataEdit._id },
		}

		await doDelete(data);
	}

	if (_id.length > 6 && editItem === null) {
		return (
			<div className="container">
				<p style={{ display: "flex", alignItems: "center" }}><RiLoader4Fill className="loading" style={{ marginRight: 5 }} /> Loading ToBuy lists</p>
			</div>
		)
	}

	document.title = `${onEditMode ? "Edit ToBuyList" : "New ToBuy List"} | BuyMe`;
	return (
		<div className="add-tobuy">
			<div className="container">
				<div className="half-flex fixed-flex">
					<div className="flex-item header-block">
						<NavLink to={`/u/${localSession && JSON.parse(localSession).uid}`} className="btn bg-white"><RiArrowLeftLine style={{ marginRight: 5 }} /> Back</NavLink>
					</div>
					<div className="flex-item header-block text-right">
						<button className="bg-success" onClick={handleSubmitMyTobuyList}
							disabled={(editIndex >= 0 && editIndex !== null) ? true : false}
						>
							{isLoading ? <RiLoader4Fill className="loading" /> : (updateThisID || onEditMode) ? "Update List" : "Submit"}
						</button>
					</div>
				</div>

				<div className="panel">
					<div className="panel-header">ToBuy Details</div>

					<div className="panel-body">
						<form onSubmit={handleSubmitMyTobuyList}>
							<div className="input-parent">
								<label>Title</label>
								<input type="text" name="title" placeholder="Groceries for Ema's birthday"
									value={formData?.title ? formData.title : (formDataEdit?.title ? formDataEdit.title : "")}
									required onChange={handleInput}
								/>
							</div>
							<div className="input-parent">
								<label>Store name</label>
								<input type="text" name="store" placeholder="Best Buy"
									value={formData?.store ? formData.store : (formDataEdit?.store ? formDataEdit.store : "")} required onChange={handleInput}
								/>
							</div>
						</form>
					</div>
				</div>

				<div className="panel">
					<div className="panel-header">
						<div className="half-flex">
							<div className="flex-item fixed-flex">
								ToBuy Items
							</div>

							{
								onEditMode ?
									(formDataEdit?.tobuys && formDataEdit.tobuys.length > 0) && <div className="flex-item fixed-flex">{formDataEdit?.tobuys.length}</div>
									:
									(formData?.tobuys && formData.tobuys.length > 0) && <div className="flex-item fixed-flex">{formData?.tobuys.length}</div>
							}
						</div>
					</div>

					<div className="panel-body tobuy-item-overflow">
						<div className="tobuy-item-list">

							{
								onEditMode ?
									formDataEdit.tobuys.map((item, key) =>
										<ToBuyItem
											key={key}
											item={item}
											modify={true}
											editIndex={editIndex}
											handleEdit={handleEdit}
											handleRemove={handleRemove}
										/>)
									:
									formData.tobuys.map((item, key) =>
										<ToBuyItem
											key={key}
											item={item}
											modify={true}
											editIndex={editIndex}
											handleEdit={handleEdit}
											handleRemove={handleRemove}
										/>)
							}

							{
								onEditMode ?
									!formDataEdit.tobuys.length &&
									<small className="color-dark">Please use the form below to add ToBuy items.</small>
									:
									!formData.tobuys.length && <small className="color-dark">Please use the form below to add ToBuy items.</small>
							}
						</div>
					</div>
				</div>

				<div className="panel" id="tobuyItemForm">
					<div className="panel-body">
						<form onSubmit={handleAddItem}>
							<div className="input-parent">
								<label>Product name</label>
								<input type="text" name="product" placeholder="Happy birthday banner"
									value={tobuyItem?.product ? tobuyItem.product : (tobuyItemEdit?.product ? tobuyItemEdit.product : "")} onChange={handleInput}
								/>
							</div>

							<div className="half-flex">
								<div className="flex-item input-parent">
									<label>Quantity</label>
									<input type="number" name="quantity" min={1}
										value={tobuyItem?.quantity ? tobuyItem.quantity : (tobuyItemEdit?.quantity ? tobuyItemEdit.quantity : "")} onChange={handleInput}
									/>
								</div>
								<div className="flex-item input-parent">
									<label>Unit</label>
									<select name="unit"
										required={true}
										value={tobuyItem?.unit ? tobuyItem.unit : (tobuyItemEdit?.unit ? tobuyItemEdit.unit : "")}
										onChange={handleInput}
									>
										<option value={""}>Select Unit</option>
										{
											units.map((unit, key) =>
												<option key={key} value={unit.name}>{unit.name}</option>)
										}
									</select>
								</div>
							</div>

							<div className="input-parent">
								<label>Product Description</label>
								<textarea type="text" name="description" placeholder="Max 200 letters." maxLength={200} rows={2} value={tobuyItem?.description ? tobuyItem.description : (tobuyItemEdit?.description ? tobuyItemEdit.description : "")} onChange={handleInput}></textarea>
							</div>

							<div>
								<button type="submit"
									className={(editIndex >= 0 && editIndex !== null) ? "bg-theme" : ""}
									onClick={handleAddItem}
								>
									{(editIndex >= 0 && editIndex !== null) ? "Update Item" : "Add Item"}
								</button>
							</div>
						</form>
					</div>
				</div>

				<div>
					<button
						style={{ marginBottom: 15 }}
						className="bg-success elem-block"
						onClick={handleSubmitMyTobuyList}
						disabled={(editIndex >= 0 && editIndex !== null) ? true : false}
					>
						{isLoading ? <RiLoader4Fill className="loading" /> : (updateThisID || onEditMode) ? "Update List" : "Submit"}
					</button>

					{
						onEditMode &&
						<>
							<button onClick={handleDelete}
								disabled={(editIndex >= 0 && editIndex !== null) ? true : false}
							>
								<small className="color-danger">Delete List</small>
							</button>
							<small className="elem-block" style={{ marginTop: 10 }}>Deleting a list can not be undone.</small>
						</>
					}
				</div>
			</div>
		</div >
	)
}

export default ToBuyForm;