import { useEffect } from "react";
import { NavLink } from "react-router-dom";
import { Tooltip } from "react-tooltip";
import { RiAddLine } from "react-icons/ri";
import { getTobuyListAPI } from "../../helpers/endpoints/tobuysAPI";
import { useSession } from "../../helpers/hooks/context/useSession";
import { useTobuy } from "../../helpers/hooks/context/useTobuy";
import { RiLoader4Fill } from "react-icons/ri";
import ToBuyListItem from "./components/ToBuyListItem";
import axios from "axios";

const MyToBuys = () => {
	const { tobuyDispatch, tobuyLists } = useTobuy();
	const { sessionDispatch, session } = useSession();

	useEffect(() => {
		const fetchUserData = async () => {
			tobuyDispatch({ type: "TOBUYLISTS", payload: null });

			await axios.get(getTobuyListAPI, { headers: { Authorization: `Bearer ${session.token}` } })
				.then(res => {
					tobuyDispatch({ type: "TOBUYLISTS", payload: [...res.data] });
				})
				.catch(err => {
					sessionDispatch({ type: "ISALERT", payload: err.request.response });
				})
		}

		if (session) {
			fetchUserData();
		}

	}, [tobuyDispatch, session, sessionDispatch]);

	if (tobuyLists === null) {
		return (
			<div className="container">
				<p style={{ display: "flex", alignItems: "center" }}><RiLoader4Fill className="loading" style={{ marginRight: 5 }} /> Loading ToBuy lists</p>
			</div>
		)
	}

	document.title = "My ToBuy | ToBuy";
	return (
		<div className="tobuy-list">
			<div className="container">
				<div className="half-flex fixed-flex">
					<NavLink to={"tobuy/add"}
						data-tooltip-id="tooltip_new-tobuy"
						data-tooltip-content="Create New ToBuy"
						data-tooltip-place="left"
						data-tooltip-float={true}
						className="btn bg-link"
					>
						New <RiAddLine style={{ marginLeft: 5 }} />
					</NavLink>
					<Tooltip id="tooltip_new-tobuy" />
				</div>

				<div style={{ marginTop: 20 }}></div>
				{
					(tobuyLists && !tobuyLists?.length) ?
						<p>Click on the New + button to add a ToBuy list.</p>
						:
						tobuyLists.map((tobuyList, index) => <ToBuyListItem key={index} tobuyList={tobuyList} />)
				}
			</div>
		</div>
	)
}

export default MyToBuys;