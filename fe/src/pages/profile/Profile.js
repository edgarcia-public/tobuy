import { countries } from "../../assets/data/countries";
import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { Tooltip } from "react-tooltip";
import { RiCheckFill, RiLogoutBoxLine, RiLoader4Fill } from "react-icons/ri";
import { useSession } from "../../helpers/hooks/context/useSession";
import { useUpdateUser } from "../../helpers/hooks/user/useUpdateUser";

const Profile = () => {
	const { doUpdate, isLoading } = useUpdateUser();
	const { sessionDispatch, userData } = useSession();
	const [formData, setFormData] = useState();
	const navigate = useNavigate();

	const handleInput = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	}

	const handleSubmit = async (e) => {
		e.preventDefault();
		await doUpdate(formData);
	}

	const handleLogout = () => {
		localStorage.removeItem("session");

		navigate("/");

		setTimeout(() => {
			sessionDispatch({ type: "DESTROY" });
		}, 200);
	}

	if (userData === null) {
		return <div className="container"><p>Loading...</p></div>
	}

	document.title = "My Profile | ToBuy"
	return (
		<div className="profile">
			<div className="container">
				<div className="panel">
					<div className="panel-header">Profile</div>
					<form onSubmit={handleSubmit}>
						<div className="panel-body">
							<div className="input-parent">
								<input type="text" className="text-capitalized" name="first_name" placeholder="First name (max 10 letters)" max={10}
									defaultValue={userData?.first_name ? userData.first_name : ""} onChange={handleInput}
								/>
							</div>

							<div className="half-flex">
								<div className="flex-item input-parent">
									<input type="email" placeholder="my@email.com" disabled value={userData?.email && userData.email} />
								</div>
								<div className="flex-item input-parent">
									<select name="country"
										defaultValue={userData?.country ? userData.country : "label"}
										onChange={handleInput}
									>
										<option disabled value={"label"}>Select Country</option>
										{Object.keys(countries).map((key, index) => <option key={index} value={countries[key].name}>{countries[key].name}</option>)}
									</select>
								</div>
							</div>
						</div>
						<div className="panel-footer">
							<div className="half-flex">
								<NavLink to={"/password-reset"} className="navlink">
									<small
										data-tooltip-id="tooltip_password-reset"
										data-tooltip-content="Will also log out your account."
										data-tooltip-place="bottom"
									>Password Reset</small>
								</NavLink>
								<Tooltip id="tooltip_password-reset" />

								<button type="submit" className="bg-success" onClick={handleSubmit} disabled={isLoading}>
									{
										isLoading ?
											<RiLoader4Fill className="loading" />
											:
											<>Update <RiCheckFill style={{ marginLeft: 5 }} /></>
									}
								</button>
							</div>
						</div>
					</form>
				</div>

				<button className="bg-danger" onClick={handleLogout}><RiLogoutBoxLine style={{ marginRight: 5 }} />Log out</button>

			</div>
		</div>
	)
}

export default Profile;