import { createContext, useReducer } from "react";

export const TobuyContext = createContext();

export const tobuyReducer = (state, action) => {
	switch (action.type) {
		case "TOBUYLISTS":
			return { ...state, tobuyLists: action.payload }
		case "NEWITEM":
			return { ...state, newItem: action.payload }
		case "EDITITEM":
			return { ...state, editItem: action.payload }
		case "RESET":
			return { ...state, tobuyList: null }
		case "ISLOADING":
			return { ...state, isLoading: action.payload }
		default:
			return { ...state }
	}
}

export const TobuyContextProvider = ({ children }) => {
	const [state, dispatch] = useReducer(tobuyReducer, {
		tobuyLists: null,
		newItem: null,
		editItem: null,
		isLoading: false,
	})

	return (
		<TobuyContext.Provider value={{ ...state, tobuyDispatch: dispatch }}>
			{children}
		</TobuyContext.Provider>
	)
}
