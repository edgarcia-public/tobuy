import axios from "axios";
import { countries } from "../assets/data/countries";
import { getUserDataAPI } from "../helpers/endpoints/usersAPI";
import { createContext, useEffect, useReducer } from "react";

export const SessionContext = createContext();

const userReducer = (state, action) => {
	switch (action.type) {
		case "SESSION":
			return { ...state, session: action.payload }
		case "USERDATA":
			return { ...state, userData: action.payload }
		case "ISALERT":
			return { ...state, isAlert: action.payload }
		case "ISLOADING":
			return { ...state, isLoading: action.payload }
		case "COUNTRIES":
			return { ...state, countries: action.payload }
		case "DESTROY":
			return { session: undefined, userData: undefined }
		default:
			return { ...state }
	}
}

export const SessionContextProvider = ({ children }) => {
	const [session, dispatch] = useReducer(userReducer, {
		session: null,
		userData: null,
		isAlert: null,
		countries: null,
		isLoading: false,
	});

	useEffect(() => {
		const countriesData = Object.keys(countries).map(key => countries[key].name);

		dispatch({ type: "COUNTRIES", payload: countriesData });

		let isDone = false;
		const localSession = localStorage.getItem("session");

		if (localSession === null) {
			dispatch({ type: "DESTROY" });
		} else {
			const localSessionData = JSON.parse(localSession);

			dispatch({ type: "SESSION", payload: { ...localSessionData } });

			const fetchUserData = async () => {
				try {
					const response = await axios.get(getUserDataAPI, { headers: { Authorization: `Bearer ${localSessionData.token}` } })

					if (!isDone) {
						dispatch({ type: "USERDATA", payload: { ...response.data.user } });
					}
				} catch (error) {
					if (!isDone) {
						dispatch({ type: "ISALERT", payload: error.request.response });

						if (error.request.response === "jwt expired") {
							localStorage.removeItem("session");
							dispatch({ type: "DESTROY" });
						}
					}
				}
			}

			fetchUserData();
		}

		return () => {
			isDone = true;
		}
	}, []);

	return (
		<SessionContext.Provider value={{ ...session, sessionDispatch: dispatch }}>
			{children}
		</SessionContext.Provider>
	)
}