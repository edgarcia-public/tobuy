import { GrCircleAlert } from "react-icons/gr";

const Alert = ({ message }) => {

	return (
		<div id="alert">
			<GrCircleAlert className="alert-icon" />
			<div className="alert-message-parent">
				<span className="alert-message-content">{message}</span>
			</div>
		</div>
	)
}

export default Alert;