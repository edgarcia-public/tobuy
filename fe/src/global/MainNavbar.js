import "../assets/css/navbar.css";
import avatar from "../assets/images/utils/user.png";
import { RiLoader4Fill, RiUserLine } from "react-icons/ri";
import { TbChecklist } from "react-icons/tb";
import { NavLink } from "react-router-dom";
import { Tooltip } from "react-tooltip";
import { useSession } from "../helpers/hooks/context/useSession";

const MainNavbar = () => {
	const { userData } = useSession();

	return (
		<nav className="mainpage-navbar">
			<div className="container">
				<div className="mainpage-navbar_grid">
					<div className="mainpage-navbar_grid-left">
						<img src={avatar} alt="User" className="user-avatar" />
						<h3 className="nogaps text-capitalized">
							{
								userData === null ?
									<RiLoader4Fill className="loading" />
									:
									userData?.first_name
										? userData.first_name
										:
										<span style={{ textTransform: "none", fontSize: "1rem" }}>Hey, your name?</span>
							}
						</h3>
					</div>

					<div className="mainpage-navbar_grid-right">
						<NavLink end to={""} className="mainpage-navbar_menu-btn"
							data-tooltip-id="tooltip_tobuy-list"
							data-tooltip-content="My ToBuy"
							data-tooltip-place="left"
							data-tooltip-float={true}
						>
							<TbChecklist className="mainpage-navbar_menu-icon" />
						</NavLink>
						<Tooltip id="tooltip_tobuy-list" />

						<NavLink end to={"profile"}
							className={`mainpage-navbar_menu-btn`}
							data-tooltip-id="tooltip_profile"
							data-tooltip-content={"Profile"}
							data-tooltip-place="left"
							data-tooltip-float={true}
						>
							<RiUserLine className="mainpage-navbar_menu-icon" />
						</NavLink>
						<Tooltip id="tooltip_profile" />
					</div>
				</div>
			</div>
		</nav >
	)
}

export default MainNavbar;