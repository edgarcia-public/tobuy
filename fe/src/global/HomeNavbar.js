import logo from "../assets/images/utils/buyme-line.png";
import avatar from "../assets/images/utils/user.png";
import { NavLink } from "react-router-dom";
import { RiShoppingCartLine } from "react-icons/ri";
import { Tooltip } from "react-tooltip";
import { useSession } from "../helpers/hooks/context/useSession";

const HomeNavbar = () => {
	const { session } = useSession();

	return (
		<nav className="sub-navbar">
			<div className="container-fluid">
				<div className="sub-navbar_grid">
					<NavLink to={"/"}><img src={logo} alt="" className="logo" /></NavLink>
					<div className="sub-navbar_links">
						{
							session ?
								<>
									<NavLink to={`/u/${session.uid}`}
										className={`mainpage-navbar_menu-btn`}
										data-tooltip-id="tooltip_profile"
										data-tooltip-content={"ToBuy List"}
										data-tooltip-place="left"
										data-tooltip-float={true}
									>
										<RiShoppingCartLine className="mainpage-navbar_menu-icon" style={{ fontSize: 18 }} />
									</NavLink>
									<Tooltip id="tooltip_profile" />

									<NavLink to={`u/${session.uid}/profile`}
										style={{ display: "flex", alignItems: "center", gap: 5 }}
										data-tooltip-content="Profile"
										data-tooltip-id="tooltip_profile"
										data-tooltip-float={true}
									>
										<img src={avatar} alt="User" className="user-avatar" />
									</NavLink>
									<Tooltip id="tooltip_profile" />
								</>
								:
								<>
									<NavLink to={"/log-in"}>Log In</NavLink>
									<NavLink to={"/sign-up"}>Sign Up</NavLink>
								</>
						}
					</div>
				</div>
			</div>
		</nav>
	)
}

export default HomeNavbar;