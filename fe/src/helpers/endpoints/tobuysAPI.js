// const TOBUY_API = "http://localhost:4000/tobuys";
const TOBUY_API = "https://tobuy-be.onrender.com/tobuys";

export const updateTobuyListAPI = `${TOBUY_API}/update-list`;

export const updateTobuyItemAPI = `${TOBUY_API}/update-item`;

export const getTobuyListAPI = `${TOBUY_API}/get-all`;

export const getOneTobuyListAPI = `${TOBUY_API}/get-one`;

export const createTobuyListAPI = `${TOBUY_API}/create`;

export const deleteTobuyListAPI = `${TOBUY_API}/delete`;