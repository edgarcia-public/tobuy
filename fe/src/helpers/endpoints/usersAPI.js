// const USER_ENDPOINT = "http://localhost:4000/users";
const USER_ENDPOINT = "https://tobuy-be.onrender.com/users";

export const getUserDataAPI = `${USER_ENDPOINT}/get-user`;

export const loginUserAPI = `${USER_ENDPOINT}/log-in`;

export const signupUserAPI = `${USER_ENDPOINT}/sign-up`;

export const updateUserAPI = `${USER_ENDPOINT}/update`;
