import axios from "axios";
import { getTobuyListAPI } from "../../endpoints/tobuysAPI";
import { useTobuy } from "../context/useTobuy";
import { useSession } from "../context/useSession";
import { useNavigate } from "react-router-dom";

export const useGetTobuyList = () => {
	const { sessionDispatch, isLoading } = useSession();
	const { tobuyDispatch, editItem, tobuyLists } = useTobuy();
	const localSession = localStorage.getItem("session");
	const navigate = useNavigate();

	const doGet = async () => {

		tobuyDispatch({ type: "EDITITEM", payload: null });
		sessionDispatch({ type: "ISLOADING", payload: true });

		if (!localSession) {
			localStorage.removeItem("session");
			sessionDispatch({ type: "ISALERT", payload: "Unauthorized access. Please log in." });
			sessionDispatch({ type: "DESTROY" });
			sessionDispatch({ type: "ISLOADING", payload: false });
			navigate("/");
		}

		if (localSession) {
			const { token } = JSON.parse(localSession);

			try {
				const response = await axios.get(getTobuyListAPI, {
					headers: { Authorization: `Bearer ${token}` }
				});

				tobuyDispatch({ type: "TOBUYLISTS", payload: { ...response.data } });
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.request.response });

				if (error.request.response === "jwt expired") {
					localStorage.removeItem("session");
					sessionDispatch({ type: "DESTROY" });
				}

				setTimeout(() => {
					navigate("/");
				}, 3000)
			}
			finally {
				sessionDispatch({ type: "ISLAODING", payload: false });
			}
		}
	}

	return { doGet, tobuyDispatch, isLoading, tobuyLists, editItem }
}