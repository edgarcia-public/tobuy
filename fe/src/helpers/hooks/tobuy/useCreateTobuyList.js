import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTobuy } from "../context/useTobuy";
import { useSession } from "../context/useSession";
import { createTobuyListAPI } from "../../endpoints/tobuysAPI";

export const useCreateTobuyList = () => {
	const { sessionDispatch, userData } = useSession();
	const { tobuyDispatch, newitem } = useTobuy();
	const [updateThisID, setUpdateThisID] = useState(null);
	const session = localStorage.getItem("session");
	const navigate = useNavigate();

	const doCreate = async (formData) => {

		sessionDispatch({ type: "ISLOADING", payload: true });
		sessionDispatch({ type: "ISALERT", payload: null });

		if (!session) {
			localStorage.removeItem("session");
			sessionDispatch({ type: "ISALERT", payload: "Unauthorized access. Please log in." });
			sessionDispatch({ type: "DESTROY" });
			sessionDispatch({ type: "ISLOADING", payload: false });
			navigate("/");
		}

		if (session && !newitem) {
			const { token } = JSON.parse(session);

			try {
				const response = await axios.post(createTobuyListAPI, { formData, updateThisID }, {
					headers: { Authorization: `Bearer ${token}` }
				})

				setUpdateThisID(response.data._id);
				tobuyDispatch({ type: "NEWITEM", payload: response.data });
				const note = updateThisID ? "Successfully updated." : "Successfully created."
				sessionDispatch({ type: "ISALERT", payload: note });
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.request.response });
			}
			finally {
				sessionDispatch({ type: "ISLOADING", payload: false });
			}
		}
	}

	return { doCreate, sessionDispatch, updateThisID, userData }
}