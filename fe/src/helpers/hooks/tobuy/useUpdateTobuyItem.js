import axios from "axios";
import { useNavigate } from "react-router-dom";
import { updateTobuyItemAPI } from "../../endpoints/tobuysAPI";
import { useTobuy } from "../context/useTobuy";
import { useSession } from "../context/useSession";

export const useUpdateTobuyItem = () => {
	const { sessionDispatch, isLoading } = useSession();
	const { tobuyDispatch } = useTobuy();
	const localSession = localStorage.getItem("session");
	const navigate = useNavigate();

	const doUpdate = async (data) => {
		sessionDispatch({ type: "ISLOADING", payload: true });

		if (!localSession) {
			localStorage.removeItem("session");
			sessionDispatch({ type: "ALERT", payload: "Unauthorized access. Please log in." });
			sessionDispatch({ type: "DESTROY" });
			navigate("/");
		}

		if (localSession) {
			const { token } = JSON.parse(localSession);

			try {
				const response = await axios.patch(updateTobuyItemAPI, { ...data }, {
					headers: { Authorization: `Bearer ${token}` },
				})
				tobuyDispatch({ type: "TOBUYLISTS", payload: [...response.data] });
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.request.response });
			}
			finally {
				sessionDispatch({ type: "ISLOADING", payload: false });
			}
		}
	}

	return { doUpdate, isLoading };
}