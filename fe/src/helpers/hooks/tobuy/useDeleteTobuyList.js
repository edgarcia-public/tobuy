import axios from "axios";
import { deleteTobuyListAPI } from "../../endpoints/tobuysAPI";
import { useTobuy } from "../context/useTobuy";
import { useSession } from "../context/useSession";
import { useNavigate } from "react-router-dom";

export const useDeleteTobuyList = () => {
	const { sessionDispatch } = useSession();
	const { tobuyDispatch } = useTobuy();
	const localSession = localStorage.getItem("session");
	const navigate = useNavigate();

	const doDelete = async (data) => {
		tobuyDispatch({ type: "ISLOADING", payload: true });
		tobuyDispatch({ type: "EDITITEM", payload: null });

		if (!localSession) {
			localStorage.removeItem("session");
			sessionDispatch({ type: "ISALERT", payload: "Unauthorized access. Please log in." });
			sessionDispatch({ type: "DESTROY" });
			sessionDispatch({ type: "ISLOADING", payload: false });
			navigate("/");
		}

		if (localSession) {
			const { token, uid } = JSON.parse(localSession);

			try {
				const response = await axios.post(deleteTobuyListAPI, data, {
					headers: { Authorization: `Bearer ${token}` }
				})

				if (response) {
					sessionDispatch({ type: "ISALERT", payload: "The ToBuy list has been deleted." });
					navigate(`/u/${uid}`);
				} else {
					sessionDispatch({ type: "ISALERT", payload: "The ToBuy list has been deleted." });
				}
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.request.response });
			}
			finally {
				sessionDispatch({ type: "ISLAODING", payload: false });
			}
		}
	}

	return { doDelete }
}