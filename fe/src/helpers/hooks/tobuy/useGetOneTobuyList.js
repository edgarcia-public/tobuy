import axios from "axios";
import { getOneTobuyListAPI } from "../../endpoints/tobuysAPI";
import { useTobuy } from "../context/useTobuy";
import { useSession } from "../context/useSession";
import { useNavigate } from "react-router-dom";

export const useGetOneTobuyList = () => {
	const { sessionDispatch } = useSession();
	const { tobuyDispatch, editItem } = useTobuy();
	const localSession = localStorage.getItem("session");
	const navigate = useNavigate();

	const doGetOne = async (data) => {

		sessionDispatch({ type: "ISLOADING", payload: true });
		tobuyDispatch({ type: "EDITITEM", payload: null });

		if (!localSession) {
			localStorage.removeItem("session");
			sessionDispatch({ type: "ISALERT", payload: "Unauthorized access. Please log in." });
			sessionDispatch({ type: "DESTROY" });
			sessionDispatch({ type: "ISLOADING", payload: false });
			navigate("/");
		}

		if (localSession && !editItem) {
			const { token } = JSON.parse(localSession);

			try {
				const response = await axios.get(`${getOneTobuyListAPI}?_id=${data} `, {
					headers: { Authorization: `Bearer ${token} ` }
				})
				tobuyDispatch({ type: "EDITITEM", payload: { ...response.data } });
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.request.response });
			}
			finally {
				sessionDispatch({ type: "ISLAODING", payload: false });
			}
		}
	}

	return { doGetOne, editItem, tobuyDispatch }
}