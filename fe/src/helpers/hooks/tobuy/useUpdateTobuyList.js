import axios from "axios";
import { useNavigate } from "react-router-dom";
import { updateTobuyListAPI } from "../../endpoints/tobuysAPI";
import { useTobuy } from "../context/useTobuy";
import { useSession } from "../context/useSession";

export const useUpdateTobuyList = () => {
	const { sessionDispatch, isLoading } = useSession();
	const { tobuyDispatch } = useTobuy();
	const localSession = localStorage.getItem("session");
	const navigate = useNavigate();

	const doUpdate = async (formDataEdit, onEditMode = false) => {

		sessionDispatch({ type: "ISLOADING", payload: true });
		sessionDispatch({ type: "ISALERT", payload: null });

		if (!localSession) {
			localStorage.removeItem("session");
			sessionDispatch({ type: "ISALERT", payload: "Unauthorized access. Please log in." });
			sessionDispatch({ type: "DESTROY" });
			navigate("/");
		}

		if (localSession) {
			const { token } = JSON.parse(localSession);

			if (formDataEdit?.updated.hasOwnProperty("_id")) {
				delete formDataEdit.updated.created_at;
				delete formDataEdit.updated._id;
			}

			// const data = {
			// 	query: { _id: id },
			// 	updated: { collapsed: !tobuy_list.collapsed }
			// }

			try {
				const response = await axios.patch(updateTobuyListAPI, { ...formDataEdit }, {
					headers: { Authorization: `Bearer ${token}`, onEditMode: onEditMode },
				})

				tobuyDispatch({ type: "TOBUYLISTS", payload: response.data });

				if (onEditMode) {
					sessionDispatch({ type: "ISALERT", payload: "ToBuy List has been updated." });
					tobuyDispatch({ type: "RESET" });
				}
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.request.response });
			}
			finally {
				sessionDispatch({ type: "ISLOADING", payload: false });
			}
		}
	}

	return { doUpdate, isLoading };
}