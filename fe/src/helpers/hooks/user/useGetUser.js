import axios from "axios";
import { getUserAPI } from "../../endpoints/usersAPI";
import { useSession } from "../context/useSession";

export const useGetUser = () => {
	const { sessionDispatch, userData } = useSession();
	const localSession = localStorage.getItem("session");

	const doGet = async () => {
		if (!localSession) return;

		sessionDispatch({ type: "ISLOADING", payload: true });

		if (localSession) {
			const { token } = JSON.parse(localSession);

			try {
				const response = await axios.get(getUserAPI, {
					headers: { Authorization: `Bearer ${token}` }
				})
				sessionDispatch({ type: "USERDATA", payload: { ...response.data.user } });
				sessionDispatch({ type: "ISLOADING", payload: false });
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.request.response });
				sessionDispatch({ type: "ISLOADING", payload: false });
			}
		}
	}

	return { doGet, userData };
}