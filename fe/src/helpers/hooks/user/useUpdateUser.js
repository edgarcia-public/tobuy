import axios from "axios";
import { useSession } from "../context/useSession";
import { updateUserAPI } from "../../endpoints/usersAPI";
import { useNavigate } from "react-router-dom";

export const useUpdateUser = () => {
	const { sessionDispatch, userData, isLoading, countries } = useSession();
	const localSession = localStorage.getItem("session");
	const navigate = useNavigate();

	const doUpdate = async (formData) => {
		let newFormData = { ...formData, first_name: formData?.first_name ? formData.first_name : userData?.first_name }

		sessionDispatch({ type: "ISLOADING", payload: true });

		if (!localSession) {
			localStorage.removeItem("session");
			sessionDispatch({ type: "ISALERT", payload: "Unauthorized access. Please log in." });
			sessionDispatch({ type: "DESTROY" });
			sessionDispatch({ type: "ISLOADING", payload: false });
			navigate("/");
		}

		// no form first_name AND no userData first_name
		if (!newFormData?.first_name && !userData.first_name) {
			sessionDispatch({ type: "ISALERT", payload: "The First Name field is required." });
			sessionDispatch({ type: "ISLOADING", payload: true });
			return;
		}

		// no form first_name BUT userData HAS first_name
		if (!newFormData?.first_name && userData.first_name) {
			newFormData.first_name = userData.first_name;
		}

		// form HAS first_name, userData doesn't matter
		const nameRegEx = /^[a-zA-Z]{3,10}$/;
		if (!nameRegEx.test(newFormData.first_name)) {
			sessionDispatch({ type: "ISALERT", payload: "The First Name should be 3 to 10 letters (only)." });
			sessionDispatch({ type: "ISLOADING", payload: true });
			return;
		}

		if (localSession) {
			const { token } = JSON.parse(localSession);

			try {
				const response = await axios.patch(updateUserAPI, { ...newFormData }, {
					headers: { Authorization: `Bearer ${token}` }
				})

				sessionDispatch({ type: "ISALERT", payload: "Profile updated." });
				sessionDispatch({ type: "USERDATA", payload: { ...response.data } });
				sessionDispatch({ type: "ISLOADING", payload: false });
			} catch (error) {
				sessionDispatch({ type: "ISALERT", payload: error.resquest.response });
				sessionDispatch({ type: "ISLOADING", payload: false });
			}
		}
	}

	return { doUpdate, isLoading, countries }
}