import axios from "axios";
import { countries } from "../../../assets/data/countries";
import { loginUserAPI } from "../../endpoints/usersAPI";
import { useSession } from "../context/useSession";

export const useLogin = () => {
	const { sessionDispatch, isLoading } = useSession();

	const doLogin = async (formData) => {
		const { email, password } = formData;

		sessionDispatch({ type: "ISLOADING", payload: true });

		if (!email || !password) {
			sessionDispatch({ type: "ISALERT", payload: "All fields are required." });
			sessionDispatch({ type: "ISLOADING", payload: false });
			return;
		}

		try {
			const response = await axios.post(loginUserAPI, formData);

			const localSession = {
				token: response.data.token,
				uid: response.data.user.uid,
			}

			// localStorage session data: token and uid
			localStorage.setItem("session", JSON.stringify(localSession));

			sessionDispatch({ type: "SESSION", payload: localSession });
			sessionDispatch({ type: "USERDATA", payload: { ...response.data.user } });
			sessionDispatch({ type: "ISALERT", payload: "Welcome back!" });

			const countriesData = Object.keys(countries).map(key => countries[key].name);
			sessionDispatch({ type: "COUNTRIES", payload: countriesData });
		} catch (error) {
			sessionDispatch({ type: "ISALERT", payload: error.request.response });
		}
		finally {
			sessionDispatch({ type: "ISLOADING", payload: false });
		}
	}

	return { doLogin, isLoading };
}