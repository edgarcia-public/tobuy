import axios from "axios";
import { signupUserAPI } from "../../endpoints/usersAPI";
import { useSession } from "../context/useSession";

export const useSignup = () => {
	const { sessionDispatch, isLoading } = useSession();

	const doSignup = async (formData) => {
		const { email, password, confirm, terms } = formData;

		sessionDispatch({ type: "ISLOADING", payload: true });

		// check for valid email address format
		const emailRegex = /^[\w.-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}$/;
		if (!emailRegex.test(email.trim())) {
			sessionDispatch({ type: "ISALERT", payload: "Invalid Email Address." });
			sessionDispatch({ type: "ISLOADING", payload: false });
			return;
		}

		// check password and confirm
		if (password.trim() !== confirm.trim()) {
			sessionDispatch({ type: "ISALERT", payload: "The Passwords didn't match." });
			sessionDispatch({ type: "ISLOADING", payload: false });
			return;
		}

		// check terms
		if (!terms) {
			sessionDispatch({ type: "ISALERT", payload: "Must agree to Terms & Use." });
			sessionDispatch({ type: "ISLOADING", payload: false });
			return;
		}

		// check password: min 8, letters, numbers (optional), no special characters
		const passwordRegex = /^(?=.*[a-zA-Z]).{8,}$/;
		if (!passwordRegex.test(password.trim())) {
			sessionDispatch({ type: "ISALERT", payload: "Password should be: minimum of 8 characters optional special characters." });
			sessionDispatch({ type: "ISLOADING", payload: false });
			return;
		}

		try {
			const response = await axios.post(signupUserAPI, formData)
			const localSession = {
				token: response.data.token,
				uid: response.data.user.uid,
			}

			// localStorage session data: token and uid
			localStorage.setItem("session", JSON.stringify(localSession));

			sessionDispatch({ type: "SESSION", payload: localSession });
			sessionDispatch({ type: "USERDATA", payload: { ...response.data.user } });
			sessionDispatch({ type: "ISALERT", payload: "Sweet! Welcome to ToBuy!" });
		} catch (error) {
			sessionDispatch({ type: "ISALERT", payload: error.request.response });
		}
		finally {
			sessionDispatch({ type: "ISLOADING", payload: false });
		}
	}
	return { doSignup, isLoading }
}