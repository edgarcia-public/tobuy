import { useContext } from "react";
import { SessionContext } from "../../../context/SessionContext";

export const useSession = () => {
	const context = useContext(SessionContext);

	if (!context) return "useContext can only be used within the SessionContextProvider";

	return context;
}