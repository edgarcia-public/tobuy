import { useContext } from "react"
import { TobuyContext } from "../../../context/TobuyContext"

export const useTobuy = () => {
	const context = useContext(TobuyContext);

	if (!context) throw Error("useTobuy must be used within TobuyContextProvider");

	return context;
}