import { Outlet } from "react-router-dom";
import MainNavbar from "../global/MainNavbar";

const MainLayout = () => {
	return (
		<section className="main-layout">
			<MainNavbar />

			<div className="main-page_outlet">
				<Outlet />
			</div>
		</section>
	)
}

export default MainLayout;