import { Outlet } from "react-router-dom";
import HomeNavbar from "../global/HomeNavbar";

const HomeLayout = () => {
	return (
		<section className="home-layout">
			<HomeNavbar />

			<Outlet />
		</section>
	)
}

export default HomeLayout